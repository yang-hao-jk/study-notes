## 3.编排基础

### 3.1 变量和数据类型

#### 3.1.1 变量与参数

Studio支持全局变量、局部变量、全局参数、局部参数。

其中，全局变量在Studio界面的画布下方设置。

![image-20230328070232914](.\imgs\1)

全局参数也与之类似。

#### 3.1.2 常见数据类型

- Boolean：True、False
- int
- float
- string
- Dict
- List
- Attachment文件：文件作为变量
- Sensitive：密码。password
- Credential：密码类。username+password

分析上图的全局变量，Number类型包括int+float，Object指Dict，Array即List。

【注意】

1.数字类参数只能用于密码框

2.全局变量和全局参数可以相互转换



#### 3.1.3 变量与参数赋值

变量定义：

- Assign：变量赋值
- eval：执行python表达式
- 命令执行的结果返回：控件返回值保存为变量

![image-20230328072134002](.\imgs\2)

变量与参数的引用：

可以通过`@{}`进行变量引用。比如：

 @{getText_ret}、@{List[2]}

【注意】

1.在引用列表元素时，注意下标不越界。

2.密码相关参数的引用，只允许应用倒密码框。

3.eval控件可以直接使用原变量名，但还是推荐@{}方式引用。



### 3.2 数据操作

数据操作其实跟python基本数据结构支持的操作基本类似，可以参考。

#### 3.2.1 字符串

字符串是一串固定的字符，以单引号或双引号括起来的任意文本。

支持的字符串操作如：

- string.split：分割
- string.join：拼接
- str[start,end]：截取
- replace：替换

![image-20230328073209958](.\imgs\3)

#### 3.2.2 列表

- listCreate：创建列表
- listAppend：追加
- listInsert：根据索引插入
- listRemove：删除，支持索引或值
- listGetIndex：根据值获取索引
- listSlice：截取
- listSort：排序
- listExtend：合并列表
- in：是否存在某元素
- clear：清空
- count：计数
- For：循环



#### 3.2.3 Dict

- dictCreate：创建字典
- dictSet：添加键值对或更新值
- dictDelete：根据键删除
- dictGet、getObjectValue：根据键获取值
- dictGetValues、dictGetKeys：获取键列表
- For：通过.keys或.values可以遍历键或者值



#### 3.2.4 Datetime

- getCurrentTime：当前时间
- datetime：指定时间
- strftime：时间转字符串
- strptime：字符串转时间
- datetime.timestamp：时间转时间戳
- datetime.localtime：时间戳转本地时间



#### 3.2.5 正则表达式

- regex_search：搜索
- regex_findall：获取全部
- match：匹配



### 3.3 控制流

#### 3.3.1 分支

通过If控件。

![image-20230328074618347](C:\Users\20958\AppData\Roaming\Typora\typora-user-images\image-20230328074618347.png)

多分支。

#### 3.3.2 循环

While循环。先判断在执行Entry。

DoWhile。先执行一次Entry再判断。

For循环。

#### 3.3.3 循环中断

continue。

break。



## 4.网页自动化

4.1 定位