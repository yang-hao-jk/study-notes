## Angular表单使用全解

### 1. 响应式表单模型 VS 模板驱动表单模型

#### 1.1 基本使用

表单处理一直是前端要解决的常见问题之一，在业务中，我们可能遇到的与表单相关的操作主要包括：

- 创建表单数据模型，跟踪数据修改
- 捕获用户输入事件，及时做出响应
- 表单验证、异步验证
- 表单布局可配置化等

而在angular中，与表单操作相关的资源与接口都被封装在`@angular/forms`下，angular把表单的数据源、验证以及用户可能需要用到的api都封装在类中，并根据不同的业务场景进行了针对性的封装处理，最后得到了在angular中最常用到的四类表单资源，也称为angular表单的四个基本构造块：

- **`FormControl`**
- **`FormGroup`**
- **`FormRecord`**
- **`FormArray`**

其中，最基本的表单类是`FormControl`，它对应着一个最基本的html表单控件，比如input。

一个最基本的使用例子：

```typescript
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-reactive-favorite-color',
  template: `
    Favorite Color: <input type="text" [formControl]="favoriteColorControl">
  `
})
export class FavoriteColorComponent {
  favoriteColorControl = new FormControl('red color');
}
```

这里面使用了一个**`formControl`**指令，因此还需要在上级模块的imports数组中导入**`ReactiveFormsModule`**来使得该指令可用。

**`formControl`**指令将表单模板与FormControl类实例之间联系了起来，FormControl内部会进行自动的表单值跟踪。

![](D:\imgs\笔记\25.png)

这是一个典型的**响应式表单模型**，即表单控件的一切行为都通过FormControl类来管理和跟踪，表单模型是结构化且不可变的，它由angular内部创建，并通过相应的api进行管理。

与之相对的，angular还支持另一种表单模型，即**模板驱动表单模型**，它通过**ngModel**指令来将表单值与组件内的值进行双向绑定。

一个使用例子：

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-template-favorite-color',
  template: `
    Favorite Color: <input type="text" [(ngModel)]="favoriteColor">
  `
})
export class FavoriteColorComponent {
  favoriteColor = '';
}
```

这里使用了一个ngModel指令，需要在上级模块的imports数组中导入**`FormsModule`**来使得该指令可用。

只能通过NgModel指令访问到FormControl instance，隔离性太强，不够灵活。

![](D:\imgs\笔记\26.png)

显然，响应式表单相比于模板驱动表单要更加灵活且强大，另外angular已经对表单行为进行了一次封装和抽象，并且全程由类实例进行管理，这样的表单解决方案也要更加的健壮，可拓展性、可复用性以及可测试性也都更高，因此在我们的项目实际开发中，基本都保持一致使用响应式的表单模型。



#### 1.2 数据流

一个表单控件最基本的数据流向包括：

1. 当用户输入值时，表单模板向表单实例发出input事件，更新表单实例的最新状态；
2. 当表单实例的值更改后，发出事件，让所有订阅该事件的表单控件更新值；

响应式表单模型是这样实现这两个行为的。

首先，当用户输入值，触发input事件，该事件将最新的表单控件值带回，**`ControlValueAccessor`**会监听该事件，并把值进一步传递给FormControl实例，FormControl实例通过setValue方法设置最新状态，并通过valueChanges发出，之后所有订阅了该事件的表单控件都会收到这个最新变化并进行更新。

`ControlValueAccessor`是表单实例与模板控件之间进行通信的桥梁，提供了诸如`writeValue`，`registerOnChanges`等接口，而valueChanges是一种多播类型的Observable，可以让所有与该表单实例相关联的控件进行订阅和同步刷新。

至于模板驱动型表单的数据流也与上面类似，只不过少了`ControlValueAccessor`这个桥梁。



